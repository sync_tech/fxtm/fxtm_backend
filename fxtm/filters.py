import django_filters
from django.db import models
from django.db import models as django_models
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from .models import Currency, Client, ExRequest, DebitTransaction, CreditTransaction, Loan, LoanTransaction, Bank, BankAccount, BankTransaction


class ClientFilter(FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Client
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'name': [],
            'vip': ['exact']

        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }
        # fields = {
        #     'started__lte': ['fecha__inicio__lte'],
        #     'started__gte': ['fecha__inicio__gte'],
        # }


class CurrencyFilter(FilterSet):
    class Meta:
        model = Currency
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }
        # fields = {
        #     'started__lte': ['fecha__inicio__lte'],
        #     'started__gte': ['fecha__inicio__gte'],
        # }


class ExRequestFilter(FilterSet):
    client__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = ExRequest
        fields = {
            # 'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'client__vip': ['exact'],
            'client__name': [],
            'is_completed': ['exact'],
            'is_supplier': ['exact'],
        }


class DebitLedgerFilter(FilterSet):
    exRequest__client__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = DebitTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'exRequest__client__name': [],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class CreditLedgerFilter(FilterSet):
    exRequest__client__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = CreditTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'exRequest__client__name': [],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class CreditByCurrencyFilter(FilterSet):
    exRequest__client__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = CreditTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'exRequest__client__name': [],
            'currency__code': ['exact']
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class LoanFilter(FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    reason = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Loan
        fields = {
            'name': [],
            'reason': [],
            'is_completed': ['exact'],
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
        }


class LoanTransactionFilter(FilterSet):

    class Meta:
        model = LoanTransaction
        fields = {
            'loan': ['exact'],
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
        }


# accounts

class DLedgerFilter(FilterSet):
    exRequest__client__name = django_filters.CharFilter(lookup_expr='icontains')

    # bank__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = DebitTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'exRequest__client__name': [],
            # 'bank__name': [],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class CLedgerFilter(FilterSet):
    exRequest__client__name = django_filters.CharFilter(lookup_expr='icontains')

    # bank__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = CreditTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'exRequest__client__name': [],
            # 'bank__name': [],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class BankAccountFilter(FilterSet):
    class Meta:
        model = BankAccount
        fields = {
            'bank': ['exact'],
        }


class BankTransactionFilter(FilterSet):
    # bank__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = BankTransaction
        fields = {
            'created_at': ['exact', 'lte', 'gte', 'lt', 'gt'],
            'account' : ['exact']
            # 'bank__name': [],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class BankAccountTestFilter(FilterSet):
    bank__name = django_filters.CharFilter(lookup_expr='icontains')
    holder = django_filters.CharFilter(lookup_expr='icontains')
    branch = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = BankAccount
        fields = {
            'holder': [],
            'branch': [],
            'bank__name': []
        }

    filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }
