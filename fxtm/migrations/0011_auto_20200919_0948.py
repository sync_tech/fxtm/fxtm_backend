# Generated by Django 3.0.7 on 2020-09-19 09:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fxtm', '0010_auto_20200919_0825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exrequest',
            name='name',
            field=models.CharField(default='2020-09-19T09:48:33.361160', max_length=30),
        ),
        migrations.AlterField(
            model_name='loantransaction',
            name='loan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='loan', to='fxtm.Loan'),
        ),
    ]
