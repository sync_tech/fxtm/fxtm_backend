from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from .views import CurrencyViewSet, ClientViewSet, ExRequestViewSet, \
    CurrencySearch, ExRequestListViewSet, DebitViewSet, CreditViewSet,\
    ExRequestCreditTransactionViewSet, ExRequestDebitTransactionViewSet, CreditLedgerViewSet, DebitLedgerViewSet, \
    CreditByCurrency, MemoViewSet, AuthorizedUserViewSet, LoanViewSet, LoanTransactionViewSet, LoanTransactionList,  LoanTakenLedgerViewSet, LoanPaidLedgerViewSet, \
    ManageUsersViewSet, ManageUsersGroupViewSet, BankViewSet, BankAccountSearch, BankAccountViewSet, BankTransactionViewSet, BankAccountSearch, TViewSet ,\
    DLedgerViewSet, CLedgerViewSet, DashboardViewSet, BankSearch, DebitWithBankViewSet



router = routers.DefaultRouter()

router.register('currencies', CurrencyViewSet)
router.register('clients', ClientViewSet)
router.register('request', ExRequestViewSet)
router.register('debits', DebitViewSet)
router.register('credits', CreditViewSet)

router.register('memo', MemoViewSet)
router.register('loan', LoanViewSet)
router.register('loan_transaction', LoanTransactionViewSet)


router.register('manage-user', ManageUsersViewSet),
router.register('banks', BankViewSet),
router.register('accounts', BankAccountViewSet),
router.register('bank_transaction', BankTransactionViewSet),

# router.register('bank_accounts', BankAccountViewSet)
# router.register('transaction', TransactionViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('token/', obtain_auth_token, name="obtain_token_auth"),

    path('user/', AuthorizedUserViewSet.as_view()),
    path('requests/', ExRequestListViewSet.as_view()),

    # path('manage-user/', ManageUsersViewSet.as_view()),
    # path('transactions', TransactionListViewSet.as_view()),
    path('dashboard', DashboardViewSet.as_view()),
    path('currency', CurrencySearch.as_view()),
    #
    path('debits_with_bank/', DebitWithBankViewSet.as_view()),
    #
    path('bank/', BankSearch.as_view()),
    path('bank_account_search', BankAccountSearch.as_view()),

    path('loan_transaction_list', LoanTransactionList.as_view()),

    path('debitLedger/', DebitLedgerViewSet.as_view()),
    path('creditLedger/', CreditLedgerViewSet.as_view()),

    path('debit/<uuid:pk>', ExRequestDebitTransactionViewSet.as_view()),
    path('credit/<uuid:pk>', ExRequestCreditTransactionViewSet.as_view()),

    path('creditByCurrency/', CreditByCurrency.as_view()),

    path('loan_taken_ledger/<uuid:pk>', LoanTakenLedgerViewSet.as_view()),
    path('loan_paid_ledger/<uuid:pk>', LoanPaidLedgerViewSet.as_view()),
    # path('t', TViewSet.as_view())

    # for accounts

    path('dLedger/', DLedgerViewSet.as_view()),
    path('cLedger/', CLedgerViewSet.as_view()),

    # path('manage-users/', .as_view()),

    # path('salesproduct/<uuid:pk>', SaleDetailView.as_view()),

]
