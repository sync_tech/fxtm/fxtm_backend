from rest_framework import permissions


# class IsRoot(permissions.BasePermission):
#     def has_permission(self, request, view):
#         if request.user and request.user.groups.filter(name='root'):
#             return True
#         # elif request.user and request.user.groups.filter(name='admin'):
#         #     return True
#         # elif request.user and request.user.groups.filter(name='personnel'):
#         #     return True
#
#         return False


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.groups.filter(name='admin'):
            return True
        # elif request.user and request.user.groups.filter(name='personnel'):
        #     return True
        return False


class IsPersonnel(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.groups.filter(name='personnel'):
            return True
        return False
