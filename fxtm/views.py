from django.db.models import Sum
from django.shortcuts import render
from rest_framework.views import APIView
from django.contrib.auth.models import Group, User
from datetime import datetime, timedelta, time
from rest_framework import filters, generics, viewsets, permissions, status
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import TokenAuthentication
from .filters import ClientFilter, ExRequestFilter, DebitLedgerFilter, CreditLedgerFilter, CreditByCurrencyFilter, \
    LoanFilter, DLedgerFilter, CLedgerFilter, BankAccountFilter, BankTransactionFilter, BankAccountTestFilter, \
    LoanTransactionFilter
from .pagination import ClientPagination, ExRequestPagination, DebitLedgerPagination, CreditLedgerPagination, \
    MemoPagination, LoanPagination, BankPagination
from .models import Currency, Client, ExRequest, CreditTransaction, DebitTransaction, Memo, Loan, LoanTransaction, Bank, \
    BankAccount, BankTransaction
from .serializers import CurrencySerializer, ClientSerializer, ExRequestSerializer, \
    ExRequestListSerializer, DebitTransactionSerializer, CreditTransactionSerializer, \
    ExDebitTransactionSerializer, ExCreditTransactionSerializer, DebitLedgerSerializer, CreditLedgerSerializer, \
    CreditResourceSerializer, MemoSerializer, LoanSerializer, LoanTransactionSerializer, LoanTransactionListSerializer, \
    TakenLoanLedgerSerializer, \
    PaidLoanLedgerSerializer, DebitTransactionWithBankSerializer, \
    GroupSerializer, UserMangeSerializer, UserGroupMangeSerializer, BankSerializer, BankAccountSerializer, \
    BankTransactionSerializer, BankAccountListSerializer

# LoanTakenLedgerSerializer, LoanPaidLedgerSerializer,  \

from .permissions import IsAdmin, IsPersonnel
from rest_framework.response import Response
from itertools import chain


# Create your views here.


class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all().order_by('created_at')
    serializer_class = CurrencySerializer


# Currency search listing api
class CurrencySearch(generics.ListAPIView):
    queryset = Currency.objects.all().order_by('name')
    serializer_class = CurrencySerializer
    search_fields = ['code', 'name']
    filter_backends = (filters.SearchFilter,)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all().order_by('-created_at')
    serializer_class = ClientSerializer
    # Adding Client Filtering
    # filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'vip']
    filter_class = ClientFilter
    # client pagination
    pagination_class = ClientPagination
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ExRequestListViewSet(generics.ListAPIView):
    queryset = ExRequest.objects.all().order_by('-updated_at')
    serializer_class = ExRequestListSerializer
    # Adding ExChange Request Filtering
    filterset_fields = ['client__vip', 'client__name', 'is_completed', 'is_supplier']
    filter_class = ExRequestFilter
    # pagination
    pagination_class = ExRequestPagination

    # permission_classes = (IsAdmin)
    # def get_permissions(self):
    #     if self.request.method == 'GET' or self.request.method == 'POST':
    #         self.permission_classes = [IsAdmin]
    #     return super(ExRequestListViewSet, self).get_permissions()
    #
    # def check_permissions(self, request):
    #     for permission in self.get_permissions():
    #         if not permission.has_permission(request, self):
    #             self.permission_denied(
    #                 request, message=getattr(permission, 'message', None)
    #             )


class ExRequestViewSet(viewsets.ModelViewSet):
    queryset = ExRequest.objects.all()
    serializer_class = ExRequestSerializer


# *** del
class DebitWithBankViewSet(generics.ListAPIView):
    queryset = DebitTransaction.objects.all()
    serializer_class = DebitTransactionWithBankSerializer


class DebitViewSet(viewsets.ModelViewSet):
    queryset = DebitTransaction.objects.all()
    serializer_class = DebitTransactionSerializer

    def partial_update(self, request, pk=None, *args, **kwargs):

        dt = DebitTransaction.objects.get(pk=pk)

        # print("*********************")
        # print("=>", dt)
        # print("==>", dt.bank_account)
        # # print("<>", dt.bank_account.id)
        # if dt.bank_account:
        #     print("===>", dt.bank_account.id)
        # print("====>", request.data.get("bank_account"))
        # print("++++++++++++++++++++")

        if dt.bank_account is not None:
            if dt.bank_account.id != request.data.get("bank_account"):
                # since the previous transaction has bank_account
                # and the bank account is not same it has to be updated(deleted)
                dt.bank_account.delete()

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        req = request.data

        if request.data.get("bank_account") is not None:
            # try:
            bank_t = BankTransaction(
                account=BankAccount.objects.get(pk=req.get("bank_account")),
                reason='debit to ' + req.get("to"),
                withdraw=req.get("amount"))
            bank_t.save()
            print(bank_t)
            # except:
            # raise exception or error message
            # return Response({"detail": "This transaction can not be save"}, status=status.HTTP_400_BAD_REQUEST)

        print("*********************")
        print(bank_t)
        print(bank_t.save())
        print(request.data)
        print("*********************")

        # serializer = self.get_serializer(instance, data=request.data, partial=partial)

        # if serializer.is_valid(raise_exception=True):
        # self.perform_update(serializer)
        # return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # return super().partial_update(request)
        return Response(status=status.HTTP_201_CREATED)

    def destroy(self, request, pk=None):
        # on delete, if the object has a bank transaction delete the transaction
        instance = self.get_object()
        if instance.bank_account is not None:
            instance.bank_account.delete()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class CreditViewSet(viewsets.ModelViewSet):
    queryset = CreditTransaction.objects.all()
    serializer_class = CreditTransactionSerializer


class ExRequestDebitTransactionViewSet(generics.RetrieveAPIView):
    queryset = ExRequest.objects.all()
    serializer_class = ExDebitTransactionSerializer
    # lookup_fExRequestSerializerields = ['id']


class ExRequestCreditTransactionViewSet(generics.RetrieveAPIView):
    queryset = ExRequest.objects.all()
    serializer_class = ExCreditTransactionSerializer
    # lookup_fields = ['exRequest']


class DebitLedgerViewSet(generics.ListAPIView):
    queryset = DebitTransaction.objects.all().order_by('-updated_at')
    serializer_class = DebitLedgerSerializer
    # filtering
    filterset_fields = ['exRequest__client__name', 'created_at']
    filter_class = DebitLedgerFilter
    # pagination
    pagination_class = DebitLedgerPagination


class CreditLedgerViewSet(generics.ListAPIView):
    queryset = CreditTransaction.objects.all().order_by('-updated_at')
    serializer_class = CreditLedgerSerializer
    # filtering
    filterset_fields = ['exRequest__client__name', 'created_at']
    filter_class = CreditLedgerFilter
    # pagination
    pagination_class = CreditLedgerPagination


# resource list and filter by currency
class CreditByCurrency(generics.ListAPIView):
    queryset = CreditTransaction.objects.all().order_by('-updated_at')
    serializer_class = CreditResourceSerializer
    # filtering
    filterset_fields = ['exRequest__client__name', 'currency__code', 'created_at']
    filter_class = CreditByCurrencyFilter
    # pagination
    pagination_class = CreditLedgerPagination


class MemoViewSet(viewsets.ModelViewSet):
    queryset = Memo.objects.all().order_by('-updated_at')
    serializer_class = MemoSerializer
    # paginations
    pagination_class = MemoPagination


class LoanViewSet(viewsets.ModelViewSet):
    queryset = Loan.objects.all().order_by('-updated_at')
    serializer_class = LoanSerializer
    # paginations
    pagination_class = LoanPagination
    filterset_fields = ['name', 'reason', 'created_at', 'is_completed']
    filter_class = LoanFilter


class LoanTransactionViewSet(viewsets.ModelViewSet):
    queryset = LoanTransaction.objects.all().order_by('-updated_at')
    serializer_class = LoanTransactionSerializer


class LoanTransactionList(generics.ListAPIView):
    queryset = LoanTransaction.objects.all().order_by('-updated_at')
    serializer_class = LoanTransactionListSerializer
    filterset_fields = ['loan', 'created_at']
    filter_class = LoanTransactionFilter


class LoanTakenLedgerViewSet(generics.RetrieveAPIView):
    queryset = Loan.objects.all()
    serializer_class = TakenLoanLedgerSerializer


class LoanPaidLedgerViewSet(generics.RetrieveAPIView):
    queryset = Loan.objects.all()
    serializer_class = PaidLoanLedgerSerializer


class AuthorizedUserViewSet(generics.RetrieveAPIView):
    queryset = Group.objects.all()

    # serializer_class = GroupSerializer

    def retrieve(self, rq):
        qs = rq.user.groups.all()
        serializer = GroupSerializer(qs, many=True)
        return Response(serializer.data)


class ManageUsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(is_staff=False)
    serializer_class = UserMangeSerializer


class ManageUsersGroupViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserGroupMangeSerializer


class BankSearch(generics.ListAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


#     # search_fields = ['name']
#     # filter_backends = (filters.SearchFilter,)
#     search_fields = ['name']
#     filter_backends = (filters.SearchFilter,)


class BankAccountSearch(generics.ListAPIView):
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountListSerializer
    # search_fields = ['name']
    # filter_backends = (filters.SearchFilter,)
    filterset_fields = ['holder', 'branch', 'bank__name']
    filter_class = BankAccountTestFilter


class BankViewSet(viewsets.ModelViewSet):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer
    pagination_class = BankPagination


class BankAccountViewSet(viewsets.ModelViewSet):
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer
    filterset_fields = ['bank']  # bank__name
    filter_class = BankAccountFilter


class BankTransactionViewSet(viewsets.ModelViewSet):
    queryset = BankTransaction.objects.all()
    serializer_class = BankTransactionSerializer

    filterset_fields = ['created_at', 'account']  # bank__name
    filter_class = BankTransactionFilter


class DLedgerViewSet(generics.ListAPIView):
    queryset = DebitTransaction.objects.all().order_by('-updated_at')
    serializer_class = DebitLedgerSerializer
    # filtering
    filterset_fields = ['exRequest__client__name', 'created_at']  # bank__name
    filter_class = DLedgerFilter


class CLedgerViewSet(generics.ListAPIView):
    queryset = CreditTransaction.objects.all().order_by('-updated_at')
    serializer_class = CreditLedgerSerializer

    filterset_fields = ['exRequest__client__name', 'created_at']  # bank__name
    filter_class = CLedgerFilter


class TViewSet(generics.ListAPIView):
    # queryset = BankTransaction.objects.all()
    serializer_class = CreditTransactionSerializer

    def get_queryset(self):
        queryset_a = CreditTransaction.objects.filter()
        queryset_b = DebitTransaction.objects.filter()

        # Create an iterator for the querysets and turn it into a list.
        results_list = list(chain(queryset_a, queryset_b))

        # print(instance)
        # Optionally filter based on date, score, etc.
        sorted_list = sorted(results_list, key=lambda instance: instance.created_at)

        # Build the list with items based on the FeedItemSerializer fields
        results = list()
        # for entry in sorted_list:
        #     item_type = entry.__class__.__name__.lower()
        #     if isinstance(entry, CreditTransaction):
        #         serializer = CreditTransactionSerializer(entry)
        #     if isinstance(entry, DebitTransaction):
        #         serializer = DebitTransactionSerializer(entry)
        #
        #     results.append({'item_type': item_type, 'data': serializer.data})
        print("hello world \n")
        print(results)
        print("\n End \n")

        return Response(results)


class DashboardViewSet(APIView):

    def get(self, request):
        data = {"ex_request": {}, "loan": {}, "balance": {}}
        # get today date time start and end
        today = datetime.now().date()
        tomorrow = today + timedelta(1)
        today_start = datetime.combine(today, time())
        today_end = datetime.combine(tomorrow, time())

        normal_ex_request = CreditTransaction.objects.all().aggregate(Sum('birr'))['birr__sum'] or 0.0
        supplier_ex_request = DebitTransaction.objects.all().aggregate(Sum('amount'))['amount__sum'] or 0.0

        normal_today_ex_request = \
            CreditTransaction.objects.filter(created_at__lte=today_end, created_at__gte=today_start).aggregate(
                Sum('birr'))[
                'birr__sum'] or 0.0
        supplier_today_ex_request = \
            DebitTransaction.objects.filter(created_at__lte=today_end, created_at__gte=today_start).aggregate(
                Sum('amount'))['amount__sum'] or 0.0

        data["ex_request"]['normal'] = normal_ex_request
        data["ex_request"]['supplier'] = supplier_ex_request
        data["ex_request"]['total'] = normal_ex_request + supplier_ex_request
        data["ex_request"]['n'] = normal_today_ex_request
        data["ex_request"]['s'] = supplier_today_ex_request

        taken_loan = LoanTransaction.objects.filter(loan_type=False).aggregate(Sum('amount'))['amount__sum'] or 0.0
        paid_loan = LoanTransaction.objects.filter(loan_type=True).aggregate(Sum('amount'))['amount__sum'] or 0.0
        today_taken_loan = LoanTransaction.objects.filter(loan_type=False, created_at__lte=today_end,
                                                          created_at__gte=today_start).aggregate(Sum('amount'))[
                               'amount__sum'] or 0.0
        today_paid_loan = LoanTransaction.objects.filter(loan_type=True, created_at__lte=today_end,
                                                         created_at__gte=today_start).aggregate(Sum('amount'))[
                              'amount__sum'] or 0.0

        data["loan"]['taken'] = taken_loan
        data["loan"]['paid'] = paid_loan
        data["loan"]['difference'] = taken_loan - paid_loan
        data["loan"]['t'] = today_taken_loan
        data["loan"]['p'] = today_paid_loan

        data["balance"]['deposit'] = 5000
        data["balance"]['withdraw'] = 1000
        data["balance"]['difference'] = 1000 + 5000
        data["balance"]['d'] = 400
        data["balance"]['w'] = 500

        return Response(data);
