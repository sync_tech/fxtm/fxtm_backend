from rest_framework import serializers
from .models import Currency, Client, ExRequest, CreditTransaction, DebitTransaction, Memo, Loan, LoanTransaction, Bank, \
    BankAccount, BankTransaction
from django.db.models import Sum
from django.contrib.auth.models import Group, User


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'


# Default Client CRUD
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class ExRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExRequest
        fields = '__all__'


class BankSerializer(serializers.ModelSerializer):
    balance = serializers.SerializerMethodField();

    def get_balance(self, req):
        # from all bank transaction calculate the current balance
        deposit = BankTransaction.objects.filter(account__bank=req).aggregate(Sum('deposit'))['deposit__sum'] or 0.0
        withdraw = BankTransaction.objects.filter(account__bank=req).aggregate(Sum('withdraw'))['withdraw__sum'] or 0.0
        balance = {"deposit": deposit, "withdraw": withdraw}
        return balance;

    class Meta:
        model = Bank
        # fields = '__all__'
        fields = ['id', 'name', 'balance', 'created_at', 'updated_at']


class BankAccountListSerializer(serializers.ModelSerializer):
    balance = serializers.SerializerMethodField('get_balance')
    holder = serializers.SerializerMethodField('get_holder')

    # balance and bank__name | holder
    def get_balance(self, obj):
        blanc = {}
        depo = BankTransaction.objects.filter(account=obj).aggregate(Sum('deposit'))['deposit__sum'] or 0.0
        witdrw = BankTransaction.objects.filter(account=obj).aggregate(Sum('withdraw'))['withdraw__sum'] or 0.0

        blanc['deposit'] = depo
        blanc['withdraw'] = witdrw
        # blanc = depo - witdrw
        return blanc

    def get_holder(self, obj):

        if obj.branch:
            return obj.holder + " | " + obj.branch + " | " + obj.bank.name
        else:
            return obj.holder + " | " + obj.bank.name

    class Meta:
        model = BankAccount
        fields = ['id', 'holder', 'branch', 'bank', 'balance', 'created_at', 'updated_at']


class BankAccountSerializer(serializers.ModelSerializer):
    balance = serializers.SerializerMethodField('get_balance')

    def get_balance(self, obj):
        blanc = {}
        depo = BankTransaction.objects.filter(account=obj).aggregate(Sum('deposit'))['deposit__sum'] or 0.0
        witdrw = BankTransaction.objects.filter(account=obj).aggregate(Sum('withdraw'))['withdraw__sum'] or 0.0

        blanc['deposit'] = depo
        blanc['withdraw'] = witdrw
        # blanc = depo - witdrw
        return blanc

    class Meta:
        model = BankAccount
        fields = ['id', 'bid', 'holder', 'branch', 'bank', 'balance', 'created_at', 'updated_at']


class ExRequestListSerializer(serializers.ModelSerializer):
    client = ClientSerializer(read_only=True)
    transactions = serializers.SerializerMethodField('get_transactions')

    def get_transactions(self, obj):
        transact = {}
        debit_transaction = DebitTransaction.objects.filter(exRequest=obj).aggregate(Sum('amount'))[
                                'amount__sum'] or 0.0
        credit_transaction = CreditTransaction.objects.filter(exRequest=obj).aggregate(Sum('birr'))[
                                 'birr__sum'] or 0.0

        transact['debitTransaction'] = debit_transaction
        transact['creditTransaction'] = credit_transaction
        return transact

    class Meta:
        model = ExRequest
        # fields = '__all__'
        fields = ['id', 'name', 'client', 'transactions', 'is_completed', 'is_supplier', 'completed_date', 'created_at',
                  'updated_at']


class CreditTransactionSerializer(serializers.ModelSerializer):
    # exRequest = ExRequestListSerializer(read_only=True)
    # currency = CurrencySerializer(read_only=True)
    class Meta:
        model = CreditTransaction
        fields = '__all__'


class DebitTransactionSerializer(serializers.ModelSerializer):
    # exRequest = ExRequestListSerializer(read_only=True)
    class Meta:
        model = DebitTransaction
        fields = '__all__'


class BankTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankTransaction
        fields = '__all__'


class DebitTransactionWithBankSerializer(serializers.ModelSerializer):
    bank_account = BankTransactionSerializer(read_only=True)

    class Meta:
        model = DebitTransaction
        fields = '__all__'


class ExDebitTransactionSerializer(serializers.ModelSerializer):
    debitTransactions = DebitTransactionWithBankSerializer(many=True)
    client = ClientSerializer(read_only=True)
    bank = BankAccountSerializer(read_only=True)

    class Meta:
        model = ExRequest
        fields = '__all__'


class ExCreditTransactionSerializer(serializers.ModelSerializer):
    creditTransactions = CreditTransactionSerializer(many=True)
    client = ClientSerializer(read_only=True)
    bank = BankAccountSerializer(read_only=True)

    class Meta:
        model = ExRequest
        fields = '__all__'


class ExRequestSerializerForLedger(serializers.ModelSerializer):
    client = ClientSerializer(read_only=True)

    class Meta:
        model = ExRequest
        fields = '__all__'


class DebitLedgerSerializer(serializers.ModelSerializer):
    exRequest = ExRequestSerializerForLedger(read_only=True)
    bank = BankAccountSerializer(read_only=True)

    class Meta:
        model = DebitTransaction
        fields = '__all__'


class CreditLedgerSerializer(serializers.ModelSerializer):
    exRequest = ExRequestSerializerForLedger(read_only=True)
    currency = CurrencySerializer(read_only=True)

    class Meta:
        model = CreditTransaction
        fields = '__all__'


class CreditResourceSerializer(serializers.ModelSerializer):
    exRequest = ExRequestSerializerForLedger(read_only=True)
    currency = CurrencySerializer(read_only=True)

    class Meta:
        model = CreditTransaction
        fields = '__all__'


class MemoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Memo
        fields = '__all__'


class LoanSerializer(serializers.ModelSerializer):
    paid_amount = serializers.SerializerMethodField('get_paid_amount')
    taken_amount = serializers.SerializerMethodField('get_taken_amount')

    def get_paid_amount(self, obj):
        paid_amount_val = LoanTransaction.objects.filter(loan=obj).filter(loan_type=True).aggregate(Sum('amount'))[
                              'amount__sum'] or 0.0
        return paid_amount_val

    def get_taken_amount(self, obj):
        taken_amount_val = LoanTransaction.objects.filter(loan=obj).filter(loan_type=False).aggregate(Sum('amount'))[
                               'amount__sum'] or 0.0
        return taken_amount_val

    class Meta:
        model = Loan
        fields = ['id', 'name', 'reason', 'taken_amount', 'paid_amount', 'is_completed', 'created_at', 'updated_at']


class LoanTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoanTransaction
        fields = '__all__'

class LoanTransactionListSerializer(serializers.ModelSerializer):
    loan = LoanSerializer()
    class Meta:
        model = LoanTransaction
        fields = '__all__'

class TakenLoanLedgerSerializer(serializers.ModelSerializer):
    # loan = LoanTransactionSerializer(many=True)
    loan = serializers.SerializerMethodField()

    class Meta:
        model = Loan
        fields = '__all__'

    def get_loan(self, obj):
        lnId = obj.id
        ln = LoanTransaction.objects.filter(loan=lnId).filter(loan_type=False)
        if ln:
            return LoanTransactionSerializer(ln, many=True).data
        else:
            return []


class PaidLoanLedgerSerializer(serializers.ModelSerializer):
    # loan = LoanTransactionSerializer(many=True)
    loan = serializers.SerializerMethodField()

    class Meta:
        model = Loan
        fields = '__all__'

    def get_loan(self, obj):
        lnId = obj.id
        ln = LoanTransaction.objects.filter(loan=lnId).filter(loan_type=True)
        if ln:
            return LoanTransactionSerializer(ln, many=True).data
        else:
            return []


# class LoanLoanTransactionSerializer(serializers.ModelSerializer):
#     loan = LoanTransactionSerializer(many=True, read_only=True)
#     paid_amount = serializers.SerializerMethodField('get_paid_amount')
#
#     def get_paid_amount(self, obj):
#         paid_amount_val = LoanTransaction.objects.filter(loan=obj).aggregate(Sum('repayment'))[
#                               'repayment__sum'] or 0.0
#         return paid_amount_val
#
#     class Meta:
#         model = Loan
#         fields = ['id', 'name', 'reason', 'taken', 'paid_amount', 'is_completed', 'loan', 'created_at', 'updated_at']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class UserMangeSerializer(serializers.ModelSerializer):
    # date_joined: "2020-08-23T19:51:44Z"
    # email: ""
    # first_name: ""
    # groups: [3]
    # 0: 3
    # id: 2
    # is_active: true
    # is_staff: false
    # is_superuser: false
    # - last_login: "2020-08-23T20:04:24Z"
    # last_name: ""
    # password: "pbkdf2_sha256$180000$1z9ahVTP3Zxm$FkS863Goouv255w4bzDoTSRoG5f9ORXYM4bapdmJVmU="
    # user_permissions: []
    # - username: "test"
    # groups = GroupSerializer(many=True)
    usr_groups = serializers.SerializerMethodField('get_groups')

    def get_groups(self, rq):
        qs = rq.groups.all()
        serializer = GroupSerializer(qs, many=True)
        return serializer.data

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'groups', 'usr_groups', 'last_login')


class UserGroupMangeSerializer(serializers.ModelSerializer):
    groups_all = GroupSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'groups', 'groups_all', 'last_login')




# class LoanTakenSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = LoanTaken
#         fields = '__all__'
#
#
# class LoanPaidSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = LoanPaid
#         fields = '__all__'


# TODO://
# Update Currencies rate
# Update Currencies amount
# pagination
# Security
# Profit Calculations
# Graph and Charts
# Resources Management
# Report
