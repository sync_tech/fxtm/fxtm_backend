from django.db import models
import uuid
import datetime
from decimal import Decimal
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MinValueValidator


# Create your models here.
class Currency(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, unique=True)
    code = models.CharField(max_length=3, unique=True)
    rate = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.000001'))])
    flag = models.ImageField(upload_to='flags/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    # https://www.worldometers.info/geography/flags-of-the-world/
    
    def __str__(self):
        return self.code


class Client(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, unique=True)
    phone = ArrayField(
        models.CharField(max_length=14, blank=True, null=True), size=5, blank=True, null=True
    )
    vip = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # boolean vip

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class ExRequest(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    name = models.CharField(max_length=30, default= datetime.datetime.now().isoformat())
    is_completed = models.BooleanField(default=False)
    is_supplier = models.BooleanField(default=False)
    completed_date = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.client.name + ' -> ' + self.name


class Bank(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class BankAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bid = models.CharField(max_length=50, null=True, blank=True)
    holder = models.CharField(max_length=50, null=True, blank=True)
    branch = models.CharField(max_length=50, null=True, blank=True)
    bank = models.ForeignKey(Bank, related_name='bank', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.holder


class BankTransaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    deposit = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(Decimal('0.01'))])
    withdraw = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(Decimal('0.01'))])
    reason = models.CharField(max_length=100, null=True, blank=True)
    account = models.ForeignKey(BankAccount, related_name='account', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class CreditTransaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    requested_amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    rate = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.1'))])
    currency = models.ForeignKey(Currency, related_name="currency", on_delete=models.PROTECT, null=False)
    birr = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.1'))])
    info = models.CharField(max_length=50, null=True, blank=True)
    exRequest = models.ForeignKey(ExRequest, related_name='creditTransactions', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class DebitTransaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    to = models.CharField(max_length=50, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    exRequest = models.ForeignKey(ExRequest,related_name='debitTransactions', on_delete=models.PROTECT)
    bank_account = models.OneToOneField(BankTransaction, null=True, blank=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.amount)


class Memo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(null=True, blank=True, default='')
    body = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    #create by


class Loan(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, null=True, blank=True)
    reason = models.TextField(null=True, blank=True, default='')
    is_completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # create by


class LoanTransaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, null=True, blank=True)
    loan_type = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    loan = models.ForeignKey(Loan, related_name='loan', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

