# Generated by Django 3.0.7 on 2020-09-03 20:27

from decimal import Decimal
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('fxtm', '0004_auto_20200818_1609'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('accountId', models.CharField(blank=True, max_length=50, null=True)),
                ('accountHolder', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='loan',
            name='is_completed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='exrequest',
            name='name',
            field=models.CharField(default='2020-09-03T20:27:43.984504', max_length=30),
        ),
        migrations.AlterField(
            model_name='loan',
            name='reason',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='memo',
            name='title',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.CreateModel(
            name='WithDraw',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('amount', models.CharField(blank=True, max_length=50, null=True)),
                ('reason', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('bank', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='withdraw_bank_account', to='fxtm.Bank')),
            ],
        ),
        migrations.CreateModel(
            name='Deposit',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('reason', models.CharField(blank=True, max_length=100, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('bank', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='deposit_bank_account', to='fxtm.Bank')),
            ],
        ),
        migrations.AddField(
            model_name='credittransaction',
            name='bank',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='fxtm.Bank'),
        ),
        migrations.AlterField(
            model_name='debittransaction',
            name='bank',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='fxtm.Bank'),
        ),
    ]
