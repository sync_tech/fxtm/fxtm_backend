from django.contrib import admin
from .models import Currency, Client, ExRequest, DebitTransaction, CreditTransaction, Memo, Loan, LoanTransaction, Bank, BankAccount, BankTransaction
# Register your models here.

admin.site.register(Currency)
admin.site.register(Client)
admin.site.register(ExRequest)

admin.site.register(DebitTransaction)
admin.site.register(CreditTransaction)

admin.site.register(Memo)
admin.site.register(Loan)
admin.site.register(LoanTransaction)
admin.site.register(Bank)
admin.site.register(BankAccount)
admin.site.register(BankTransaction)